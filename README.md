# HSGR Ansible #

Ansible playbook and roles for managing HSGR infrastructure.

## License ##

[![license](https://img.shields.io/badge/AGPLv3-6672DB.svg)](LICENSE)
[![Hackerspace.gr](https://img.shields.io/badge/%C2%A9%202019-Hackerspace.gr-6672D8.svg)](https://hackerspace.gr/)
